﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Management;
using System.Text.RegularExpressions;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Commtools
{
    /// <summary>
    /// 配置本机的IP环境。包括IP,SubnetMask,Gateway,DNS
    /// 需在调用处判断参数是否IP地址规范，因为本类不会提示报错。
    /// </summary>
    public class IPHelper
    {
        /// <summary>
        /// 设置DNS
        /// </summary>
        /// <param name="dns"></param>
        public static void SetDNS(string[] dns)
        {
            SetIPAddress(null, null, null, dns);
        }
        /// <summary>
        /// 设置网关
        /// </summary>
        /// <param name="getway"></param>
        public static void SetGetWay(string getway)
        {
            SetIPAddress(null, null, new string[] { getway }, null);
        }
        /// <summary>
        /// 设置网关
        /// </summary>
        /// <param name="getway"></param>
        public static void SetGetWay(string[] getway)
        {
            SetIPAddress(null, null, getway, null);
        }

        /// <summary>
        /// 设置IP地址和掩码
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="submask"></param>
        public static void SetIPAddress(string ip, string submask)
        {
            SetIPAddress(new string[] { ip }, new string[] { submask }, null, null);
        }
        /// <summary>
        /// 设置IP地址，掩码和网关
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="submask"></param>
        /// <param name="getway"></param>
        public static void SetIPAddress(string ip, string submask, string getway)
        {
            SetIPAddress(new string[] { ip }, new string[] { submask }, new string[] { getway }, null);
        }
        /// <summary>
        /// IP地址，掩码，网关,首选DNS
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="submask"></param>
        /// <param name="getway"></param>
        /// <param name="dns"></param>
        public static void SetIPAddress(string ip, string submask, string getway, string dns)
        {
            SetIPAddress(new string[] { ip }, new string[] { submask }, new string[] { getway }, new[] { dns });
        }
        /// <summary>
        /// 设置IP地址，掩码，网关和DNS
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="submask"></param>
        /// <param name="getway"></param>
        /// <param name="dns"></param>
        public static void SetIPAddress(string[] ip, string[] submask, string[] getway, string[] dns)
        {
            ManagementClass wmi = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection moc = wmi.GetInstances();
            ManagementBaseObject inPar = null;
            ManagementBaseObject outPar = null;
            foreach (ManagementObject mo in moc)
            {
                //如果没有启用IP设置的网络设备则跳过
                if (!(bool)mo["IPEnabled"])
                    continue;
                //设置IP地址和掩码
                if (ip != null && submask != null)
                {
                    inPar = mo.GetMethodParameters("EnableStatic");
                    inPar["IPAddress"] = ip;
                    inPar["SubnetMask"] = submask;
                    outPar = mo.InvokeMethod("EnableStatic", inPar, null);
                }
                //设置网关地址
                if (getway != null)
                {
                    inPar = mo.GetMethodParameters("SetGateways");
                    inPar["DefaultIPGateway"] = getway;
                    outPar = mo.InvokeMethod("SetGateways", inPar, null);
                }
                //设置DNS地址
                if (dns != null)
                {
                    inPar = mo.GetMethodParameters("SetDNSServerSearchOrder");
                    inPar["DNSServerSearchOrder"] = dns;
                    outPar = mo.InvokeMethod("SetDNSServerSearchOrder", inPar, null);
                }
            }
        }

        /// <summary>
        /// 启用DHCP服务器
        /// </summary>
        public static void EnableDHCP()
        {
            ManagementClass wmi = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection moc = wmi.GetInstances();
            foreach (ManagementObject mo in moc)
            {
                //如果没有启用IP设置的网络设备则跳过
                if (!(bool)mo["IPEnabled"])
                    continue;
                //重置DNS为空
                mo.InvokeMethod("SetDNSServerSearchOrder", null);
                //开启DHCP
                mo.InvokeMethod("EnableDHCP", null);
            }
        }

        /// <summary>
        /// 判断是否符合网络地址格式
        /// ip,subnetMask,gateway，dns本质上都是网络地址，所以也可以验证
        /// </summary>
        /// <param name="netAddress">IP地址</param>
        /// <returns>是否符合格式</returns>
        public static bool IsNetAddress(string netAddress)
        {
            #region 旧代码，字符串匹配方法
            //string[] arr = ip.Split('.');
            //if (arr.Length != 4)
            //    return false;
            //string pattern = @"/d{1,3}";
            //for (int i = 0; i < arr.Length; i++)
            //{
            //    string d = arr[i];
            //    if (i == 0 && d == "0")
            //        return false;
            //    if (!Regex.IsMatch(d, pattern))
            //        return false;
            //    if (d != "0")
            //    {
            //        d = d.TrimStart('0');
            //        if (d == "")
            //            return false;
            //        if (int.Parse(d) > 255)
            //            return false;
            //    }
            //}
            //return true; 
            #endregion
            string rxIP = @"^((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$";
            if (Regex.IsMatch(netAddress, rxIP))
                return true;
            else
                return false;
        }

        /// <summary>
        /// 获取客户端Ip地址
        /// 若客户端使用代理等技术可能获取不到、不正确，ip为空串
        /// </summary>
        /// <returns>客户端Ip地址</returns>
        public static string GetequestIp()
        {
            HttpRequest request = HttpContext.Current.Request;
            string clientIp = request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (clientIp.IsNullOrWhiteSpace()) clientIp = request.ServerVariables["REMOTE_ADDR"];
            if (clientIp.IsNullOrWhiteSpace()) clientIp = request.UserHostAddress;
            if (clientIp.IsNullOrWhiteSpace()) clientIp = string.Empty;
            return clientIp;
        }

        /// <summary>
        /// 获取本机ip
        /// </summary>
        public static string GetLocalIp()
        {
            //在某些机器上，[0]包含IP地址，[1]会数组越界出错。采用下方安全代码
            //string localIp = Dns.GetHostAddresses(Dns.GetHostName())[1].ToString();  
            return Dns.GetHostAddresses(Dns.GetHostName()).First(ip => ip.AddressFamily == AddressFamily.InterNetwork).ToString();
        }

    }
}
