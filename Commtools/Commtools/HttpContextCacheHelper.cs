﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace Commtools
{
    /// <summary>
    /// IIS本地缓存
    /// </summary>
    public class HttpContextCacheHelper
    {
        public static void Clear()
        {
            IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Remove(enumerator.Key.ToString());
            }
        }

        public static object Get(string cacheKey)
        {
            return HttpRuntime.Cache[cacheKey];
        }

        public static T Get<T>(string cacheKey)
        {
            return (T)HttpRuntime.Cache[cacheKey];
        }

        public static List<object> GetAll()
        {
            List<object> list = new List<object>();
            IDictionaryEnumerator enumerator = HttpContext.Current.Cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                list.Add(Get(enumerator.Key.ToString()));
            }
            return list;
        }

        public static void Remove(string cacheKey)
        {
            HttpRuntime.Cache.Remove(cacheKey);
        }

        public static void Save(string cacheKey, object cacheObject)
        {
            HttpRuntime.Cache.Insert(cacheKey, cacheObject);
        }

        public static void Save(string cacheKey, object cacheObject, CacheDependency dependency)
        {
            HttpRuntime.Cache.Insert(cacheKey, cacheObject, dependency);
        }

        /// <summary>
        /// 保存缓存
        /// </summary>
        /// <param name="cacheKey">缓存键</param>
        /// <param name="cacheObject">缓存值</param>
        /// <param name="absoluteExpiration">绝对过期时间，比如60秒后则为DateTime.Now.AddSeconds(60)</param>
        public static void Save(string cacheKey, object cacheObject, DateTime absoluteExpiration)
        {
            HttpRuntime.Cache.Insert(cacheKey, cacheObject, null, absoluteExpiration, Cache.NoSlidingExpiration);
        }

        public static void Save(string cacheKey, object cacheObject, CacheDependency dependency, DateTime absoluteExpiration, TimeSpan slidingExpiration)
        {
            HttpRuntime.Cache.Insert(cacheKey, cacheObject, dependency, absoluteExpiration, slidingExpiration);
        }

        public static void Save(string cacheKey, object cacheObject, CacheDependency dependency, DateTime absoluteExpiration, TimeSpan slidingExpiration, CacheItemUpdateCallback onUpdateCallback)
        {
            HttpRuntime.Cache.Insert(cacheKey, cacheObject, dependency, absoluteExpiration, slidingExpiration, onUpdateCallback);
        }

        public static void Save(string cacheKey, object cacheObject, CacheDependency dependency, DateTime absoluteExpiration, TimeSpan slidingExpiration, CacheItemPriority priority, CacheItemRemovedCallback onRemoveCallback)
        {
            HttpRuntime.Cache.Insert(cacheKey, cacheObject, dependency, absoluteExpiration, slidingExpiration, priority, onRemoveCallback);
        }

    }
}
