﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Commtools
{
    /// <summary>
    /// 静态类,用于扩展方法
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// 去除指定子串
        /// </summary>
        /// <param name="str">待操作的字符串</param>
        /// <param name="substring">指定的子串</param>
        public static string RemoveSubString(this string str, string substring)
        {
            return str.Replace(substring, "");
        }

        /// <summary>
        /// 去除末尾的字符串
        /// </summary>
        /// <param name="str">待操作的字符串</param>
        /// <param name="splitFlag">应去除的末尾字符串</param>
        /// <returns>返回处理后的字符串</returns>
        public static string RemoveLastSplitFlag(this string str, string splitFlag)
        {
            int i = str.LastIndexOf(splitFlag);
            if (i == -1)    //不存在末尾标志位
            {
                return str;
            }
            else
            {
                return str.Remove(i, splitFlag.Length);
            }
        }

        /// <summary>
        /// 计算字符串中特定子串的个数。例如原串"aaa",子串"aa",函数返回1
        /// </summary>
        /// <param name="str">待操作的字符串</param>
        /// <param name="substring">待计数的子串</param>
        /// <returns></returns>
        public static int CountSubString(this string str, string substring)
        {
            return Regex.Matches(str, substring).Count;
        }

        /// <summary>
        /// 根据传进来的分割符将字符串分割
        /// </summary>
        /// <param name="str">需要分割的字符串</param>
        /// <param name="splitStr">分隔符</param>
        /// <returns>返回分割好的字符串数组</returns>
        public static List<string> StringSplit(this string str, string splitStr)
        {
            if (str.IsNullOrWhiteSpace() || str.Replace(splitStr, string.Empty).IsNullOrWhiteSpace())   return new List<string>();
            if (splitStr.IsNullOrWhiteSpace()) return new List<string> {str};

            var strArr = str.Split(new[] {splitStr}, StringSplitOptions.RemoveEmptyEntries);
            return strArr.IsListNullOrEmpty() ? new List<string>() : strArr.ToList();
        }

        /// <summary>
        /// 获取字符串两标志之间的的字符值
        /// </summary>
        /// <param name="str">原字符串</param>
        /// <param name="begMark">开始标志</param>
        /// <param name="endMark">结束标志</param>
        /// <returns>字符值，若不存在标志则为null</returns>
        public static string GetValueBetweenMarks(this string str, string begMark, string endMark)
        {
            if (str.Contains(begMark) && str.Contains(endMark))
            {
                int indexBeg = str.IndexOf(begMark) + begMark.Length;
                int length = str.IndexOf(endMark) - indexBeg;
                string value = str.Substring(indexBeg, length);
                return value;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 判断字符串是否全部包含特定关键字.
        /// </summary>
        /// <param name="str">父字符串</param>
        /// <param name="keys">关键字</param>
        /// <returns>含有所有关键字则返回true，不含一个或全部不含则返回false</returns>
        public static bool ContainsKeys(this string str, params string[] keys)
        {
            foreach (var item in keys)
            {
                if (!str.Contains(item))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// 判断字符串是否为空串或NULL或只含空白符
        /// </summary>
        public static bool IsNullOrWhiteSpace(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

        /// <summary>
        /// 判断字符串是否不为空串或NULL或不只含空白符
        /// </summary>
        public static bool IsNotNullOrWhiteSpace(this string str)
        {
            return !str.IsNullOrWhiteSpace();
        }

        /// <summary>
        /// 判断该整数是否为零 
        /// </summary>
        public static bool IsZero(this int num)
        {
            return num == 0;
        }

        /// <summary>
        /// 判断该整数是否处于指定的起止范围内，含等于
        /// </summary>
        /// <param name="num">要判断的数字</param>
        /// <param name="begNum">小数</param>
        /// <param name="endNum">大数</param>
        /// <returns></returns>
        public static bool IsBetween(this int num, int begNum, int endNum)
        {
            if (begNum > endNum)
                throw new Exception("begValue 大于 endValue");
            return (num >= begNum && num <= endNum);
        }

        /// <summary>
        /// 整型字符串转整数
        /// </summary>
        /// <param name="num">待转换的字符串数字</param>
        /// <param name="failDafaultValue">转换失败的默认值</param>
        /// <returns></returns>
        public static int ToInt(this string num, int failDafaultValue = 0)
        {
            int convertNum;
            return int.TryParse(num, out convertNum) ? convertNum : failDafaultValue;
        }

        /// <summary>
        /// 整型字符串转长整数
        /// </summary>
        /// <param name="num">待转换的字符串数字</param>
        /// <param name="failDafaultValue">转换失败的默认值</param>
        /// <returns></returns>
        public static long ToLong(this string num, int failDafaultValue = 0)
        {
            long convertNum;
            return long.TryParse(num, out convertNum) ? convertNum : failDafaultValue;
        }

        public static double ToDouble(this string num, double failDafaultValue = 0)
        {
            double convertNum;
            return double.TryParse(num, out convertNum) ? convertNum : failDafaultValue;
        }

        public static float ToFloat(this string num, float failDafaultValue = 0)
        {
            float convertNum;
            return float.TryParse(num, out convertNum) ? convertNum : failDafaultValue;
        }

        public static decimal ToDecimal(this string num, decimal failDafaultValue = 0)
        {
            decimal convertNum;
            return decimal.TryParse(num, out convertNum) ? convertNum : failDafaultValue;
        }

        public static DateTime ToDateTime(this string dtStr, int defaultYear = 1900, int defaultMonth = 1, int defaultDay = 1)
        {
            DateTime dt;
            return DateTime.TryParse(dtStr, out dt) ? dt : new DateTime(defaultYear, defaultMonth, defaultDay);
        }

        /// <summary>
        ///字符串转布尔值
        /// </summary>
        /// <param name="num">待转换的字符串，必须为true或者false</param>
        /// <param name="failDafaultValue">转换失败的默认值</param>
        /// <returns></returns>
        public static bool ToBool(this string str, bool failDafaultValue = false)
        {
            bool result;
            return bool.TryParse(str, out result) ? result : failDafaultValue;
        }
   
        /// <summary>
        /// 限制字符串长度。若字符串长度大于指定参数，则替代参数代替
        /// </summary>
        /// <param name="str">原字符串</param>
        /// <param name="maxCount">限制最大长度，为0则不限制长度返回原文</param>
        /// <param name="abbr">最大长度后的字符用此省略符代替</param>
        /// <returns>返回带省略符的字符串</returns>
        public static string AbbreviateString(this string str, int maxCount, string abbr = "...")
        {
            if (maxCount == 0) return str;

            if (str.Length > maxCount)
            {
                str = str.Substring(0, maxCount) + abbr;
            }
            return str;
        }

        /// <summary>
        /// 获取枚举的描述信息，若无描述信息则直接返回枚举常量
        /// </summary>
        /// <param name="e">枚举值</param>
        /// <returns>枚举值上定义的Description属性，若无描述信息则直接返回枚举常量</returns>
        public static string GetEnumDesc(this Enum e)
        {
            try
            {
                var enumInfo = e.GetType().GetField(e.ToString());
                var enumAttributes = (DescriptionAttribute[])enumInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
                return enumAttributes.Length > 0 ? enumAttributes[0].Description : e.ToString();
            }
            catch
            {
                return e.ToString();
            }
        }

        /// <summary>
        /// 元素是否等于null
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool IsNull(this object obj)
        {
            return obj == null;
        }

        /// <summary>
        /// 元素是否等于null
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static bool IsListNullOrEmpty<T>(this IEnumerable<T> list)
        {
            return (list == null) || (!list.Any());
        }

        /// <summary>
        /// 转换为枚举
        /// 调用示例：int i = 1; ValidEnum e = i.ToEnum~ValidEnum~();  ~表示左右尖括号
        /// </summary>
        /// <typeparam name="T">枚举类型</typeparam>
        /// <param name="iEnum">枚举值，整形</param>
        public static T ToEnum<T>(this int iEnum)
        {
            return Enum.IsDefined(typeof(T), iEnum) ? (T)Enum.Parse(typeof(T), iEnum.ToString()) : default(T);
        }

        /// <summary>
        /// StringBuilder去除前后指定字符
        /// </summary>
        /// <param name="sb">StringBuilder字符</param>
        /// <param name="trimChars">待从原字符去除的前后指定字符</param>
        public static StringBuilder Trim(this StringBuilder sb, params char[] trimChars)
        {
            return new StringBuilder(sb.ToString().Trim(trimChars));
        }

        /// <summary>
        /// DateTime为空类型赋初值
        /// </summary>
        public static DateTime GetDateTime(this DateTime? dt, int defaultYear = 1900, int defaultMonth = 1, int defaultDay = 1)
        {
            return dt.HasValue ? dt.Value : new DateTime(defaultYear, defaultMonth, defaultDay);
        }

        /// <summary>
        /// Json序列化
        /// </summary>
        public static string ToJson(this object obj)
        {
            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore});
        }

        /// <summary>
        /// 将对象中值为null的string类型成员赋值为string.Empty
        /// 该方法可返回，不反回亦可。参数是引用类型、直接改变对象
        /// </summary>
        public static T AssignNullStringToEmpty<T>(this T obj)
        {
            foreach (var property in obj.GetType().GetProperties())
            {
                if (property.PropertyType == typeof(string) && property.GetValue(obj, null).IsNull())
                {
                    property.SetValue(obj, string.Empty, null);
                }
            }
            return obj;
        }

        /// <summary>
        /// 是否是数字，包括：正数负数、小数
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNumeric(this string value)
        {
            return Regex.IsMatch(value, @"^[+-]?\d*[.]?\d*$");
        }

        /// <summary>
        /// 是否是整数，包括正数负数。不验证大小（即不验证超出int范围的情况）
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsInt(this string value)
        {
            return Regex.IsMatch(value, @"^[+-]?\d*$");
        }

        /// <summary>
        /// 转成字符串，若为null则string.Empty
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToStringWithNull(this object obj)
        {
            return obj == null ? string.Empty : obj.ToString();
        }
    }
}
