﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Commtools
{
    /// <summary>
    /// 公共方法，不便分类的方法放在此处
    /// </summary>
    public class UtilHelper
    {
        /// <summary>
        /// 获取调用本方法的方法名称
        /// </summary>
        /// <param name="index">调用本方法的上级方法调用链深度。例如：调用链关系：FunA=》FunB=》GetCallFunctionName，则index = 1返回FunB,index = 2返回FunA，index = 3返回本身GetCallFunctionName</param>
        public static string GetCallFunctionName(int index = 1)
        {
            return new StackTrace(true).GetFrame(index).GetMethod().Name;
        }

        /// <summary>
        /// 获取当前方法名
        /// 若在ClassA.FunctionB中调用本方法，则返回FunctionB
        /// </summary>
        /// <returns></returns>
        public static string GetInstanceFuctionName()
        {
            return new StackTrace().GetFrame(1).GetMethod().Name;
        }

        /// <summary>
        /// 获取当前类名
        /// 若在ClassA.FunctionB中调用本方法，则返回ClassA
        /// </summary>
        /// <returns></returns>
        public static string GetInstanceClassName()
        {
            return new StackTrace().GetFrame(1).GetMethod().ReflectedType.Name;
        }

        /// <summary>
        /// Http Url编码
        /// 引入System.Web.Mvc
        /// </summary>
        public static string UrlEncode(string str)
        {
            return Uri.EscapeDataString(str);
        }

        /// <summary>
        /// Http Url解码
        /// 引入System.Web.Mvc
        /// </summary>
        public static string UrlDecode(string str)
        {
            return HttpUtility.UrlDecode(str);
        }

        /// <summary>  
        /// 获取时间戳
        /// TODO:具体实现、原理未明确
        /// </summary>  
        /// <returns></returns>  
        public static string GetTimeStamp()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds).ToString();
        }

        /// <summary>
        /// 去除Html标签，生成摘要
        /// </summary>
        /// <param name="html">html文字，含标签</param>
        /// <param name="maxCount">限制最大长度，为0则不限制长度返回原文</param>
        /// <param name="abbr">最大长度后的字符用此省略符代替</param>
        /// <returns></returns>
        public static string ReplaceHtmlTag(string html, int maxCount = 20, string abbr = "...")
        {
            if (string.IsNullOrWhiteSpace(html)) return string.Empty;
            int index = html.IndexOf("src=\"data:image");
            if (index > 0 && index < 20)
            {
                return string.Empty;
            }
            html = html.Trim();
            string tmp = Regex.Replace(html, "\\s*|\t|\r|\n", "");
            tmp = Regex.Replace(tmp, "<[^>]+>", "");
            string result = Regex.Replace(tmp, "&[^;]+;", "");

            if (maxCount == 0) return result;
            return result.AbbreviateString(maxCount, abbr);
        }
    }
}
