﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commtools
{
    /// <summary>
    /// 字符串格式化形式，包括日期，数字格式
    /// DateTime默认格式：2014/12/28 15:44:51
    /// </summary>
    public class StringFormat
    {
        /// <summary>
        /// 日期格式化，形如：2014-12-28
        /// </summary>
        public const string DateFormat = "yyyy-MM-dd";

        /// <summary>
        /// 日期时间格式化，形如：2014-12-28 15:44:51
        /// </summary>
        public const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
    }
}
