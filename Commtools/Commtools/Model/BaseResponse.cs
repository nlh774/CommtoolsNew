﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Commtools.Model
{
    /// <summary>
    /// 泛型响应
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseResponse<T>
    {
        public BaseResponse()
        {
        }

        public BaseResponse(T t)
        {
            Content = t;
        }

        public BaseResponse(T t, int code, string message)
        {
            Content = t;
            Code = code;
            Message = message;
        }

        public T Content { get; set; }

        public int Code { get; set; }

        public string Message { get; set; }

        /// <summary>
        /// 根据用户身份与业务逻辑，返回url
        /// 不一定有值。有值时建议页面跳转到该路径，无值时按默认跳转（比如主页）
        /// </summary>
        public string ReturnUrl { get; set; }
    }

    /// <summary>
    /// 非泛型响应
    /// </summary>
    public class BaseResponse
    {
        public BaseResponse()
        {
        }

        public BaseResponse(int code, string message)
        {
            Code = code;
            Message = message;
        }

        public int Code { get; set; }

        public string Message { get; set; }

        /// <summary>
        /// 根据用户身份与业务逻辑，返回url
        /// 不一定有值。有值时建议页面跳转到该路径，无值时按默认跳转（比如主页）
        /// </summary>
        public string ReturnUrl { get; set; }
    }
}