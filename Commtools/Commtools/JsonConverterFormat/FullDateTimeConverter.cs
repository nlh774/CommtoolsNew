﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using TCSmartFramework.Utility;

namespace Commtools.JsonConverterFormat
{
    /// <summary>
    /// DateTime Json序列化格式：yyyy-MM-dd HH:mm:ss
    /// </summary>
    public class FullDateTimeConverter : DateTimeConverterBase
    {
        private static IsoDateTimeConverter dtConverter = new IsoDateTimeConverter { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" };

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return dtConverter.ReadJson(reader, objectType, existingValue.ToStringWithNull().ToDateTime(), serializer);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            dtConverter.WriteJson(writer, value.ToStringWithNull().ToDateTime(), serializer);
        }
    }
}
