﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Commtools
{
    /// <summary>
    /// 有效状态
    /// </summary>
    public enum AvailableState
    {
        [Description("有效")]
        Valid = 0,

        [Description("无效")]
        InValid = 1
    }

    /// <summary>
    /// Http请求类型
    /// 主要是Get,Post
    /// </summary>
    public enum HttpMethod
    {
        [Description("Get")]
        Get = 0,

        [Description("Post")]
        Post = 1
    }

    /// <summary>
    /// 性别
    /// </summary>
    public enum Gender
    {
        [Description("男")]
        Male = 0,

        [Description("女")]
        Female = 1
    }

    /// <summary>
    /// 是否
    /// </summary>
    public enum YesNo
    {
        [Description("否")]
        No = 0,

        [Description("是")]
        Yes = 1
    }
}
