﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Commtools
{
    public enum ApiCodeEnum
    {
        Success = 0,
        AccountError = 1,
        ParamError = 2,
        VerifyFail = 3,
        DataNotExisted = 4,
        NoAuth = 5,
        NetworkTimeout = 6,
        /// <summary>
        /// 数据有依赖项
        /// 一般用在删除操作前的确认
        /// </summary>
        DataDependencyRelation = 7,
        NeedVerifyQuesAnsw = 8,
        ServerRunError = 100,
    }
}