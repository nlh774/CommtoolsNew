﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Commtools.Test_Web.Models
{
    /// <summary>
    /// 数据字典
    /// 主要用于数据库动态配置枚举类型，比如：ProfessionType，可以定义枚举：Teacher、Doctor、IT...，这样代码就写死、每次新增都得改代码。如若配置在本数据字典中，则只要做个CRUD页面操作数据，然后var types = service.GetDataDictionary(1000).1000是职业类型id
    /// </summary>
    public class DataDictionaryModel
    {
        public long Id { get; set; }

        public long ParentId { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// 字典键
        /// 长整形。在DB中也可以进行LIKE '%searchKeyParam%'
        /// </summary>
        public long Key { get; set; }

        /// <summary>
        /// 字典值
        /// 原则上只有两级。第一级 目录：1000~9999。第二季 目录：1000下属10000001、10000002~10009999，1001下属10010001、10010002、10010003......第一级第二级可分9999种类别（第一级实际仅8999个类别）
        /// </summary>
        public string Value { get; set; }
    }
    

    public class DataDictionaryService
    {
        public List<DataDictionaryModel> GetChildrenByParentId(long parentId)
        {
            return new List<DataDictionaryModel>();
        }

        public string GetNameByValue(string value)
        {
            return string.Empty;
        }
    }
}