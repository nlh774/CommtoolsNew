﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Web;
using Newtonsoft.Json;

namespace Commtools.Test_Web.Models
{
    /// <summary>
    /// 操作日志，包括操作者、内容、时间、详细信息、筛选主键、调用方法、模块名
    /// </summary>
    public class SystemRunLog
    {
        public static void Log(SystemRunLogModel log)
        {
            //log.CallFunctionName = log.CallFunctionName.IsNullOrWhiteSpace() ? UtilHelper.GetCallFunctionName(2) : log.CallFunctionName;
            //ThreadPool.QueueUserWorkItem(t =>   //使用线程异步保存，加速执行速度
            //{
            //    DbContext.OperateLogs.Add(log);
            //});
        }

        public static void Log(SystemRunLogType operateLogType, string msg, string keyFilter1, string operatorUser, string category = "", string subCategory = ""
            , Dictionary<string, string> extraInfo = null, string keyFilter2 = "", string callFunctionName = "")
        {
            //插入操作日志表
            var model = new SystemRunLogModel
            {
                Type = operateLogType,
                Message = msg,
                KeyFilter1 = keyFilter1,
                KeyFilter2 = keyFilter2,
                Category = category,
                SubCategory = subCategory,
                CallFunctionName = callFunctionName.IsNullOrWhiteSpace() ? UtilHelper.GetCallFunctionName(2) : callFunctionName,
                ExtraInfo = JsonConvert.SerializeObject(extraInfo).AbbreviateString(1000),
                OperatorUser = operatorUser,
            };
            Log(model);
        }
    }

    public enum SystemRunLogType
    {
        /// <summary>
        /// 普通，主要用于记录日常操作日志
        /// </summary>
        Info,
        /// <summary>
        /// 警报，主要用于记录日常危险的操作日志、或者警戒点阀值
        /// </summary>
        Warn,
        /// <summary>
        /// 错误异常，主要用于记录已确认的程序异常日志
        /// </summary>
        Error,
        /// <summary>
        /// 调试测试日志，基本可忽略
        /// </summary>
        Debug,
    }

    public class SystemRunLogModel
    {
        public SystemRunLogModel()
        {
            Type = SystemRunLogType.Info;
            Message = KeyFilter1 = KeyFilter2 = Category = SubCategory = CallFunctionName = ExtraInfo = OperatorUser = string.Empty;
            //公共字段默认值
        }


        public long Id { get; set; }

        public SystemRunLogType Type { get; set; }

        public string Message { get; set; }

        public string KeyFilter1 { get; set; }

        public string KeyFilter2 { get; set; }

        public string Category { get; set; }

        public string SubCategory { get; set; }

        /// <summary>
        /// 调用方法名，若不指定将自动获取
        /// </summary>
        public string CallFunctionName { get; set; }

        /// <summary>
        /// 额外信息，统一格式：Dictionary(string, string)（例如：{{"字段1","值1"},{"字段2","值2"}}）转换为Json
        /// 数据库该字符串最长1000
        /// </summary>
        public string ExtraInfo { get; set; }

        public string OperatorUser { get; set; }

        //其他公共字段:IsDel,Remark,CreatedOn,CreatedBy
        
    }
}