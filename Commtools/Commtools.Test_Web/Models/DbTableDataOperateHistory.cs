﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Commtools.Test_Web.Models
{
    /// <summary>
    /// 数据库表操作历史细节
    /// 详细记录某用户、某时间、对某表、某主键、某字段的修改（新旧值）
    /// </summary>
    public class DbTableDataOperateHistory
    {
        /// <summary>
        /// 操作表表名
        /// </summary>
        public string TableName { get; private set; }

        /// <summary>
        /// 操作表主键
        /// </summary>
        public long TableKeyId { get; private set; }

        /// <summary>
        /// 操作类型
        /// </summary>
        public OperateType Type { get; private set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; private set; }

        /// <summary>
        /// 操作者id
        /// </summary>
        public long UserId { get; private set; }

        /// <summary>
        /// 操作者姓名
        /// </summary>
        public string UserName { get; private set; }

        /// <summary>
        /// 字段新旧值修改详细日志
        /// </summary>
        public List<FieldUpdateLogContent> FieldUpdateLogContents { get; private set; }


        /// <summary>
        /// 构造
        /// </summary>
        public DbTableDataOperateHistory(string tableName, long tableKeyId, OperateType type, string remark = "", long userId = 0, string userName = "")
        {
            TableName = tableName;
            TableKeyId = tableKeyId;
            Type = type;
            Remark = remark ?? string.Empty;
            UserId = userId;
            UserName = userName ?? string.Empty;
            FieldUpdateLogContents = new List<FieldUpdateLogContent>();
        }

        /// <summary>
        /// 添加字段新旧值修改详细日志
        /// </summary>
        public List<FieldUpdateLogContent> AddLog(FieldUpdateLogContent log)
        {
            FieldUpdateLogContents.Add(log);
            return FieldUpdateLogContents;
        }

        ///// <summary>
        ///// 记录新旧实体修改
        ///// 要求：实体内的成员均是简单类型、且不含方法、字段中文名描述用DisplayAttribute属性的Name字段定义否则日志中文名为空
        ///// </summary>
        ///// <typeparam name="T">泛型</typeparam>
        ///// <param name="oldObj">旧实体</param>
        ///// <param name="newObj">新实体</param>
        //public List<FieldUpdateLogContent> AddLog<T>(T oldObj, T newObj)
        //{
        //    var type = oldObj.GetType();
        //    foreach (var prop in type.GetProperties())
        //    {
        //        if ((prop.GetCustomAttributes(typeof(IgnoreAttribute), false)).Length > 0)
        //            continue;   //忽略部分字段

        //        var attrs = (DisplayAttribute[])prop.GetCustomAttributes(typeof(DisplayAttribute), false);
        //        string cnName = attrs.Length > 0 ? attrs[0].Name : string.Empty;
        //        string engName = prop.Name;
        //        object oldValue = prop.GetValue(oldObj);
        //        object newValue = prop.GetValue(newObj);
        //        if (JsonConvert.SerializeObject(oldValue) != JsonConvert.SerializeObject(newValue)) //用json类型比较，更准确。虽然都是简单类型用ToString()应该也问题不大
        //            FieldUpdateLogContents.Add(new FieldUpdateLogContent(engName, cnName, oldValue.ToString(), newValue.ToString()));
        //    }
        //    return FieldUpdateLogContents;
        //}

        ///// <summary>
        ///// 保存修改历史
        ///// </summary>
        //public void SaveOperationLog()
        //{
        //    ThreadPool.QueueUserWorkItem(t =>   //使用线程异步保存，加速执行速度
        //    {
        //        //插入操作日志表
        //        var model = new OperationLogModel
        //        {
        //            TableName = TableName,
        //            TableKeyId = TableKeyId,
        //            Type = Type.GetHashCode(),
        //            Content = (Type == OperateType.Update && !FieldUpdateLogContents.IsListNullOrEmpty()) ? JsonConvert.SerializeObject(FieldUpdateLogContents).AbbreviateString(1990) : string.Empty,
        //            Creator = UserName,
        //            CreateTime = DateTime.Now,
        //            IsDel = BooleanEnum.No.GetHashCode(),
        //            Remark = Remark,
        //        };
        //        if (!CommonDAL.Add(model))
        //            LogFactory.GetLogger("SkyLog").Error(new Exception("保存详细修改历史失败"), "保存详细修改历史失败", "SaveOperationLog", TableName,
        //                null, "", "", TableKeyId.ToString());
        //    });
        //}
    }


    /// <summary>
    /// 字段新旧值修改详细日志
    /// </summary>
    public class FieldUpdateLogContent
    {
        /// <summary>
        /// 字段英文名，一般就是表的列名
        /// </summary>
        public string EnName { get; private set; }

        /// <summary>
        /// 字段中文名，一般就是表的中文描述名
        /// </summary>
        public string CNName { get; private set; }

        /// <summary>
        /// 旧值
        /// </summary>
        public string OldValue { get; private set; }

        /// <summary>
        /// 新值
        /// </summary>
        public string NewValue { get; private set; }


        /// <summary>
        /// 构造
        /// </summary>
        public FieldUpdateLogContent(string enName, string cnName, string oldValue, string newValue)
        {
            EnName = enName;
            CNName = cnName;
            OldValue = oldValue;
            NewValue = newValue;
        }
    }


    /// <summary>
    /// 操作类型
    /// </summary>
    public enum OperateType
    {
        [Description("新增")]
        Add = 0,

        [Description("更新")]
        Update = 1,

        [Description("逻辑删除")]
        Remove = 2,
    }
}
